export function changeName(username) {
    return {
        type: "CHANGE_NAME",
        username
    };
}

export function change(myname){
    return{
        type:"CHANGE",
        myname
    }
}