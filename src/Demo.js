import React, { Component } from 'react';

class Demo extends Component{
    constructor(props){
        super(props);
        this.state = {
          title: "React Demo",
          time: "123"
        }
    }

    _changeTitle(){
      this.setState({title:"React Demo 2"});
    }

    componentWillMount(){
    }

    componentDidMount(){
    }

    render(){
        return(
            <div>
                <h3>{this.state.title}</h3>
                <p>{"Xin chào: "+this.props.data}</p>
                <button onClick={this._changeTitle.bind(this)}>Thay đổi state</button>
            </div>
        )
    }
}

export default Demo;