import React,{Component} from 'react';
import { connect }          from 'react-redux';
import {change}         from './actions';

class Input extends Component{

    constructor(props){
        super(props);
        this._change = this._change.bind(this);
    }

    _change(){
        this.props.dispatch(change("Nguyễn Công Đào"))
    }

    render(){
        return(
            <div>
                <input type="text" name="yourname"/>
                <button onClick={this._change}>Show</button>
            </div>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
	};
}
export default connect(_mapStateToPropsTop)(Input);