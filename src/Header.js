import React, { Component } from 'react';
import { connect }          from 'react-redux';

import {changeName}         from './actions';

class Header extends Component{
    constructor(props){
        super(props);
        this._changeName = this._changeName.bind(this);
    }

    componentWillMount(){
    }

    _changeName(){
        this.props.dispatch(changeName("Lê Hoàng Anh"));
    }

    componentDidMount(){
    }

    render(){
        return(
            <div>
                <input type="text" name="yourname" />
                <button onClick={this._changeName}>Lưu</button>
            </div>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
	};
}
export default connect(_mapStateToPropsTop)(Header);