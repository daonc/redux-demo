import React, { Component } from 'react';

class Main extends Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
    }

    componentDidMount(){
    }

    render(){
        return(
            <div>
                <p>{"Tên của bạn: "+this.props.data}</p>
            </div>
        )
    }
}

export default Main;