import React,{Component} from 'react';

class Text extends Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return (
            <div>
                <p>{"My name: "+this.props.name}</p>
            </div>
        )
    }
}

export default Text;