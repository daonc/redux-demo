import React, { Component } from 'react';
import { connect }          from 'react-redux';

import Header   from './Header';
import Main     from './Main';
import Test     from './Test.js';
import Text from './Text.js';
import Input from './Input.js';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      yourname : "Chưa nhập"
    }
  }

  componentWillMount(){
  }

  componentDidMount(){
  }

  render() {
    return (
      <div className="App">
        <Header />
        <Main data = {this.props.username} />
        <Test />
        <Input />
        <Text name={this.props.myname}/>
      </div>
    );
  }
}

function _mapStateToPropsTop(state) {
	return {
      username: state.username,
      myname: state.myname
	};
}
export default connect(_mapStateToPropsTop)(App);
